#!/bin/sh
rm Pipfile Pipfile.lock
pipenv --python 3.8
pipenv install -r requirements.txt
