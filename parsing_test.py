import unittest


# import the required libraries
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pickle
import os.path
import base64
import email
from bs4 import BeautifulSoup
import pathlib

import logging
import email_parser as ep


class Testing(unittest.TestCase):
    def setUp(self) -> None:
        self.credentials = ep.getToken(pathlib.Path(__file__).parent.resolve())
        self.service = build(
            'gmail', 'v1', credentials=self.credentials)

    def test_get_thread(self):
        self.assertFalse(False)
        threads = ep.getThreadList(self.service)
        self.assertIsInstance(threads, dict)

    def test_get_number_of_threads(self):
        self.assertEqual(0, ep.getNumberOfThreads(
            ep.getThreadList(self.service)))

    def test_get_messages(self):
        threads = ep.getThreadList(self.service)
        messages = []
        if ep.getNumberOfThreads(threads) > 0:
            for thread in threads["threads"]:
                thread_id = thread["id"]
                messages.extend(ep.getThreadMessages(self.service, thread_id))
        print(messages)

    def test_get_body(self):
        threads = ep.getThreadList(self.service)
        if ep.getNumberOfThreads(threads) > 0:
            for thread in threads["threads"]:
                thread_id = thread["id"]

        print(ep)

    def test_get_emails_from_sender(self):
        self.assertFalse(False)

    def test_get_yes_response(self):
        self.assertFalse(False)

    def test_get_no_response(self):
        self.assertFalse(False)

    def test_get_duplicates(self):
        self.assertFalse(False)

    # def test_connection_to_gmail_api(self):
        # credentials = getToken(pathlib.Path(__file__).parent.resolve())
        # service = build('gmail', 'v1', credentials=credentials)


if __name__ == '__main__':
    unittest.main()
