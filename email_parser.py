
# import the required libraries
import base64
import logging
import pandas as pd
import pathlib
import pickle
from bs4 import BeautifulSoup
from datetime import datetime
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.discovery import build


class RSVPResponse():
    def __init__(self) -> None:
        self.last_name = None
        self.first_name = None
        self.email = None
        self.is_coming = None
        self.number_of_extra_guests = None
        self.comment = None


def getToken(credentials_path: str, scopes=['https://www.googleapis.com/auth/gmail.readonly']):
    credentials = None
    if not pathlib.Path.exists(credentials_path):
        logging.warning("credentials path does not exist")
        return None
    token_open_flags = 'rb'
    if pathlib.Path.exists(credentials_path / "token.pickle"):
        with open('token.pickle', token_open_flags) as token:
            credentials = pickle.load(token)

    if not credentials or not credentials.valid:
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
        elif pathlib.Path.exists(credentials_path / 'credentials.json'):

            flow = InstalledAppFlow.from_client_secrets_file(
                credentials_path / 'credentials.json', scopes)
            credentials = flow.run_local_server(port=0)

        # Save the access token in token.pickle file for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(credentials, token)

    return credentials


def getThreadList(service) -> dict:
    threads = service.users().threads().list(userId='me',
                                             q="from:organizer@angiexshehabwedding.com").execute()
    return threads


def getNumberOfThreads(threads: dict):
    if "threads" in threads.keys():
        return len(threads["threads"])
    return 0


def getThreadMessages(service, thread_id):
    thread = service.users().threads().get(
        userId='me', id=thread_id, format="full").execute()
    return thread["messages"]


def getBodyOfMessage(message):
    body = message["payload"]["body"]["data"]
    body = body.replace("-", "+").replace("_", "/")
    soup = BeautifulSoup(base64.b64decode(body), "lxml")
    return soup


def main():
    credentials = getToken(pathlib.Path(__file__).parent.resolve())
    service = build('gmail', 'v1', credentials=credentials)
    getThreadList(service)
    threads = getThreadList(service)
    messages = []
    if getNumberOfThreads(threads) > 0:
        for thread in reversed(threads["threads"]):
            thread_id = thread["id"]
            thread_msgs = getThreadMessages(service, thread_id)
            messages.extend(thread_msgs)

    message_bodies = []
    for message in messages:
        message_bodies.append(getBodyOfMessage(message))

    data = []
    for message in message_bodies:
        table = message.select(
            "#templateBody > table > tbody > tr > td > table > tbody > tr > td > table")
        msg_data = []
        for entry in table:
            msg_data.append(entry.select(
                "tbody > tr:nth-child(2) > td")[0].text)
        data.append(msg_data)
    
    d = [d for d in data if len(d) <= 5]
    df_complete_old = pd.DataFrame(
        d, columns=["name", "email", "attending", "plus", "team"])
    df_complete_old["msg"] = ""

    d = [d for d in data if len(d)== 6]
    df_complete_new = pd.DataFrame(
        d, columns=["name", "email", "attending", "plus", "team", "msg"])

    df_complete = pd.concat([df_complete_old, df_complete_new])

    df_complete["plus"].replace({"None, just me.": "+0"}, inplace=True)
    df_complete['plus'] = df_complete['plus'].map(
        lambda name: name.split("+")[-1]).astype(int)
    df_complete['total_guests'] = df_complete['plus'] + 1
    not_attending = df_complete[df_complete["attending"] == "No"]
    attending_df = df_complete[df_complete["attending"] == "Yes"]

    print(f"the attendees are: {attending_df.total_guests.sum()}")
    current_time = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    with pd.ExcelWriter(f"{pathlib.Path(__file__).parent.resolve()}/guests_{current_time}.xlsx") as writer:
        attending_df.to_excel(writer, sheet_name='attending')
        not_attending.to_excel(writer, sheet_name='not_attending')
        df_complete.to_excel(writer, "full_list")


if __name__ == "__main__":
    main()
